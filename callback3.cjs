const fs = require('fs');
const path = require('path');

const cardData = path.resolve('cards.json');

function problem3(id, cb) {

    setTimeout(() => {

        fs.readFile(cardData, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                //console.log(data);
                const cardObjectData = JSON.parse(data);

                // console.log(cardDataKeys);
                cb(null, cardObjectData[id]);
            }
        });
    }, 2 * 1000);


}
module.exports = problem3;