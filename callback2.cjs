const fs = require('fs');
const path = require('path');

const listData = path.resolve('lists.json');

function problem2(boradDataId, cb) {

    setTimeout(() => {

        fs.readFile(listData, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                //console.log(data);

                const boradObjectData = JSON.parse(data);
                const listDataKeys = Object.keys(boradObjectData);
                // console.log(boradData);
                // boradData.map((boardItem) => {
                listDataKeys.map((item) => {
                    if (item == boradDataId)
                        cb(null, boradObjectData[item]);
                });
                //});

            }
        })

    }, 2 * 1000);

}
module.exports = problem2;